To check out and build
=================================
sudo apt install git-buildpackage
git clone https://anonscm.debian.org/cgit/collab-maint/blackbird-gtk-theme.git
cd blackbird-gtk-theme
git checkout -b upstream/latest origin/upstream/latest
git checkout master
gbp buildpackage

# If the latest version is from a different git branch, you'll also need to run
# this command. Remember to checkout master before building.
git checkout -b upstream/gtk3.20 origin/upstream/gtk3.20

Set up upstream remote
======================
# Add these lines to the bottom of .git/config (in your repository)

[remote "upstream"]
        url = https://github.com/shimmerproject/Blackbird/
        fetch = +refs/heads/*:refs/remotes/upstream/*
        fetch = +refs/tags/*:refs/tags/upstream/*
        tagopt = --no-tags

Merge a new release tag
=======================
# Demonstration with the v3.18.0 tag
git checkout upstream/latest
# This fetches the upstream tag and adds upstream/ to the front of it
git fetch upstream v3.18.0
git pull upstream master
git checkout master
dch -v3.18.0-1
git add debian/changelog
debcommit -e
gbp buildpackage

Release
=======
dch -r
gbp buildpackage --git-tag --git-sign-tags

# Verify that everything looks ok before pushing to git
git push --all --dry-run
git push --all
git push --tags --dry-run
git push --tags

Build a git snapshot
====================
git checkout upstream/latest
git pull upstream master

# You need to use a tag for gbp buildpackage to work.
# The format is existing release +date.7-digit git commit id, so for instance:
git tag upstream/v3.18.0+20160721.06bd49a
git checkout master

# Update debian/changelog.
# Use the same tag as above and append -1 for an initial Debian release
# or -0ubuntu1 for an initial Ubuntu release.
dch -v3.18.0+20160721.06bd49a-1
git add debian/changelog
debcommit -e
gbp buildpackage

Build a git snapshot from a different branch
============================================
# To initially create the branch, run this:
git checkout -b upstream/gtk3.20 upstream/gtk3.20

git checkout upstream/gtk3.20
git pull upstream gtk3.20

# Tag like the previous section but follow with a pointer to the upstream git branch name
git tag upstream/v3.18.0+20160720.7b72fea.gtk320
git checkout master

dch -v3.18.0+20160720.7b72fea.gtk320-1
git add debian/changelog
debcommit -e
gbp buildpackage
